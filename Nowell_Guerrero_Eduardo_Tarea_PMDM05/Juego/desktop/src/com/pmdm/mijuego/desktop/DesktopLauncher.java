package com.pmdm.mijuego.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.pmdm.mijuego.MiJuego;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		//Altura
		config.width=800;
		//Ancho
		config.height=480;
		//Titulo
		config.title="Mazmorras";
		new LwjglApplication(new MiJuego(), config);
	}
}
