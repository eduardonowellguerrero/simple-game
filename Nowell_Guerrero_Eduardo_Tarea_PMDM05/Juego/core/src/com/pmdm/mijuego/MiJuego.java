package com.pmdm.mijuego;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import java.util.Timer;
import java.util.TimerTask;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;



public class MiJuego extends ApplicationAdapter implements InputProcessor {
	int t = 0;
	int p = 0;
	//Objeto que recoge el mapa de baldosas
	private TiledMap mapa;
	//Objeto con el que se pinta el mapa de baldosas
	private OrthogonalTiledMapRenderer mapaRenderer;
	// Cámara que nos da la vista del juego
	private OrthographicCamera camara;
	// Atributo en el que se cargará la hoja de sprites del mosquetero.
	private Texture img;
	//Atributo que permite dibujar imágenes 2D, en este caso el sprite.
	private SpriteBatch sb;

	//Constantes que indican el número de filas y columnas de la hoja de sprites.
	private static final int FRAME_COLS = 4;
	private static final int FRAME_ROWS = 4;

	//Animación que se muestra en el método render()
	private Animation jugador;
	//Animaciones para cada una de las direcciones de movimiento del personaje del jugador.
	private Animation jugadorArriba;
	private Animation jugadorDerecha;
	private Animation jugadorAbajo;
	private Animation jugadorIzquierda;
	// Tamaño del mapa de baldosas.
	private int mapaAncho, mapaAlto;
	//Atributos que indican la anchura y la altura de un tile del mapa de baldosas
	int anchoCelda,altoCelda;
	//Posición actual del jugador.
	private float jugadorX, jugadorY;
	// Este atributo indica el tiempo en segundos transcurridos desde que se inicia la animación
	// , servirá para determinar cual es el frame que se debe representar.
	private float stateTime;

	//Contendrá el frame que se va a mostrar en cada momento.
	private TextureRegion cuadroActual;

	private Animation noJugadorDerecha;
	private Animation noJugadorIzquierda;

	private Animation[] noJugador;
	int anchoNoJugador, altoNoJugador;
	private float[] noJugadorX;
	private float[] noJugadorY;
	private float[] posOriginal;
	private static final int numeroNPCs = 4;
	private float stateTimeNPC;
	private int ar [] = {0,1,0,1};

	private boolean [][] obstaculo;
	private TiledMapTileLayer capaObstaculos;
	int anchoJugador,altoJugador;

	private Music colision;
	private Music musica2;
	private Music musicaFondo;
	private TiledMapTileLayer capaOro;
	private boolean [][] oro;
	int total = 0;

	@Override
	public void create() {

		camara = new OrthographicCamera(800, 480);
		camara.position.set(camara.viewportWidth / 2f, camara.viewportHeight / 2f, 0);
		Gdx.input.setInputProcessor(this);
		camara.update();
		img = new Texture(Gdx.files.internal("protagonista.png"));

		//Sacamos frames del personaje
		TextureRegion[][] tmp = TextureRegion.split(img, img.getWidth() / FRAME_COLS, img.getHeight() / FRAME_ROWS);

		// Creamos animaciones por linea
		jugadorArriba = new Animation(0.150f, tmp[3]);
		jugadorDerecha = new Animation(0.150f, tmp[2]);
		jugadorAbajo = new Animation(0.150f, tmp[0]);
		jugadorIzquierda = new Animation(0.150f, tmp[1]);
		//En principio se utiliza la animación del jugador arriba como animación por defecto.
		jugador = jugadorAbajo;
		// Posición inicial del jugador.
		jugadorX = 0;
		jugadorY = 400;
		stateTime = 0f;

		sb = new SpriteBatch();

		//Cargamos el mapa
		mapa = new TmxMapLoader().load("mapa.tmx");
		mapaRenderer = new OrthogonalTiledMapRenderer(mapa);
		//Determinamos alto y ancho de las baldosas
		TiledMapTileLayer capa = (TiledMapTileLayer) mapa.getLayers().get(2);
		anchoCelda = (int) capa.getTileWidth();
		altoCelda = (int) capa.getTileHeight();
		mapaAncho = capa.getWidth() * anchoCelda;
		mapaAlto = capa.getHeight() * altoCelda;

		capaObstaculos = (TiledMapTileLayer) mapa.getLayers().get(1);
		int anchoCapa = capaObstaculos.getWidth(),altoCapa = capaObstaculos.getHeight();
		obstaculo = new boolean[anchoCapa][altoCapa];
		for(int x = 0;x<anchoCapa;x++){
			for(int y = 0;y<altoCapa;y++){
				obstaculo[x][y] = (capaObstaculos.getCell(x,y) != null);
			}
		}

		anchoJugador = ((TextureRegion) jugador.getKeyFrame(0f)).getRegionWidth();
		altoJugador = ((TextureRegion) jugador.getKeyFrame(0f)).getRegionWidth();

		noJugador = new Animation[numeroNPCs];
		noJugadorX = new float[numeroNPCs];
		noJugadorY = new float[numeroNPCs];
		posOriginal = new float[numeroNPCs];

		img = new Texture(Gdx.files.internal("soldado.png"));
		tmp = TextureRegion.split(img, img.getWidth() / FRAME_COLS, img.getHeight() / FRAME_ROWS);


		noJugadorDerecha = new Animation(0.150f, tmp[2]);
		noJugadorDerecha.setPlayMode(Animation.PlayMode.LOOP);
		noJugadorIzquierda = new Animation(0.150f, tmp[1]);
		noJugadorIzquierda.setPlayMode(Animation.PlayMode.LOOP);

		//Cargamos en los atributos del ancho y alto del sprite del mostruo sus valores
		anchoNoJugador = ((TextureRegion)noJugadorDerecha.getKeyFrame(0f)).getRegionWidth();
		altoNoJugador = ((TextureRegion)noJugadorDerecha.getKeyFrame(0f)).getRegionHeight();
		//Posicion de los NPC
		int posX [] = {100,40,448,608};
		int posY [] = {100,200,270,176};
		for(int i = 0;i<numeroNPCs;i++) {
			noJugadorX[i] = posX[i];
			noJugadorY[i] = posY[i];
			posOriginal[i] = noJugadorX[i];
			noJugador[i] = noJugadorDerecha;
		}
		stateTimeNPC = 0f;
		musica2 = Gdx.audio.newMusic(Gdx.files.internal("succes.wav"));
		colision = Gdx.audio.newMusic(Gdx.files.internal("gameover.wav"));
		musicaFondo = Gdx.audio.newMusic(Gdx.files.internal("dungeon.mp3"));
		musicaFondo.play();
		//Creamos la capa del oro
		capaOro = (TiledMapTileLayer) mapa.getLayers().get(2);
		oro = new boolean[anchoCapa][altoCapa];
		for(int x = 0;x<anchoCapa;x++){
			for(int y = 0;y<altoCapa;y++){
				oro[x][y] = (capaOro.getCell(x,y) != null);
			}
		}


	}

	@Override
	public void render() {
		//Ponemos el color del fondo a negro
		Gdx.gl.glClearColor(0, 0, 0, 1);
		//Borramos la pantalla
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		//Antes de actualizar la camara,
		camara.position.set(jugadorX, jugadorY, 0f);
		camara.position.x = MathUtils.clamp(camara.position.x, camara.viewportWidth / 2f,
				mapaAncho - camara.viewportWidth / 2f);
		camara.position.y = MathUtils.clamp(camara.position.y, camara.viewportHeight / 2f,
				mapaAlto - camara.viewportHeight / 2f);

		camara.update();
		//Vinculamos el objeto de dibuja el TiledMap con la cámara
		mapaRenderer.setView(camara);
		//Dibujamos el TiledMap
		mapaRenderer.render();
		// extraemos el tiempo de la última actualización del sprite y la acumulamos a stateTime.
		stateTime += Gdx.graphics.getDeltaTime();
		//Extraermos el frame que debe ir asociado a al momento actual.
		cuadroActual = (TextureRegion) jugador.getKeyFrame(stateTime);
		// le indicamos al SpriteBatch que se muestre en el sistema de coordenadas
		// específicas de la cámara.
		sb.setProjectionMatrix(camara.combined);
		//Inicializamos el objeto SpriteBatch
		sb.begin();
		//Pintamos el objeto Sprite a través del objeto SpriteBatch
		sb.draw(cuadroActual, jugadorX, jugadorY);
		//Dibujamos NPC
		stateTimeNPC += Gdx.graphics.getDeltaTime();
		for(int i = 0;i<numeroNPCs;i++){
			actualizaNPC(i,0.5f);
			cuadroActual = (TextureRegion) noJugador[i].getKeyFrame(stateTimeNPC);
			sb.draw(cuadroActual,noJugadorX[i],noJugadorY[i]);
		}
		//Finalizamos el objeto SpriteBatch
		sb.end();
		detectaColisiones();
		detectaOro();

	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		stateTime = 0;

		if (keycode == Input.Keys.LEFT) {
			jugadorX += -5;
			jugador = jugadorIzquierda;

		}
		if (keycode == Input.Keys.RIGHT) {
			jugadorX += 5;
			jugador = jugadorDerecha;
		}
		if (keycode == Input.Keys.UP) {
			jugadorY += 5;
			jugador = jugadorArriba;
		}
		if (keycode == Input.Keys.DOWN) {
			jugadorY += -5;
			jugador = jugadorAbajo;
		}

		//Si pulsamos la tecla del número 1, se alterna la visibilidad de la primera capa
		//del mapa de baldosas.
		if (keycode == Input.Keys.NUM_1)
			mapa.getLayers().get(0).setVisible(!mapa.getLayers().get(0).isVisible());
		//Si pulsamos la tecla del número 2, se alterna la visibilidad de la segunda capa
		//del mapa de baldosas.
		if (keycode == Input.Keys.NUM_2)
			mapa.getLayers().get(1).setVisible(!mapa.getLayers().get(1).isVisible());
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// Vector en tres dimensiones que recoge las coordenadas donde se ha hecho click
		// o toque de la pantalla.
		Vector3 clickCoordinates = new Vector3(screenX, screenY, 0);
		// Transformamos las coordenadas del vector a coordenadas de nuestra cámara.
		Vector3 posicion = camara.unproject(clickCoordinates);

		//Se pone a cero el atributo que marca el tiempo de ejecución de la animación,
		//provocando que la misma se reinicie.
		stateTime = 0;
		float jugadorAnteriorX = jugadorX;
		float jugadorAnteriorY = jugadorY;
		if ((jugadorY + altoJugador) < posicion.y) {
			jugadorY += 5;
			jugador = jugadorArriba;
		} else if ((jugadorY) > posicion.y) {
			jugadorY -= 5;
			jugador = jugadorAbajo;
		}
		if ((jugadorX + anchoJugador/2) < posicion.x) {
			jugadorX += 5;
			jugador = jugadorDerecha;
		} else if ((jugadorX - anchoJugador/2) > posicion.x) {
			jugadorX -= 5;
			jugador = jugadorIzquierda;
		}
		if((jugadorX < 0 || jugadorY < 0 ||
				jugadorX > (mapaAncho - anchoJugador) ||
				jugadorY > (mapaAlto - altoJugador)) ||(obstaculo[(int) ((jugadorX + anchoJugador/4) / anchoCelda)][((int)(jugadorY) / altoCelda)]) || (obstaculo[(int)((jugadorX + 3*anchoJugador/4) / anchoCelda)][(int)(jugadorY) / altoCelda])){
			jugadorX = jugadorAnteriorX;
			jugadorY = jugadorAnteriorY;
		}
		return true;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	private void detectaColisiones(){
		Rectangle rJugador = new Rectangle(jugadorX,jugadorY,anchoJugador,altoJugador);
		Rectangle rNPC;

		for (int i=0; i<numeroNPCs; i++) {
			rNPC = new Rectangle(noJugadorX[i], noJugadorY[i], anchoNoJugador,altoNoJugador);
			//Se comprueba si se solapan.
			if (rJugador.overlaps(rNPC)){
				t++;
				if(t == 1) {
					colision.play();
					new Timer().schedule(new TimerTask() {
						@Override
						public void run() {
							System.exit(0);
						}
					}, 1500);
				}

			}
		}
	}

	private void actualizaNPC(int i, float delta) {
		boolean obs = ((obstaculo[(int) ((noJugadorX[i] + anchoNoJugador/4) / anchoCelda)][((int) (noJugadorY[i]) / altoCelda)])
				|| (obstaculo[(int) ((noJugadorX[i] + 3*anchoNoJugador/4) / anchoCelda)][((int) (noJugadorY[i]) / altoCelda)]));

		if(ar[i] == 0 || ar[i]%2==0){
			noJugadorX[i] += delta;
			noJugador[i] = noJugadorDerecha;
		}
		if(ar[i] != 0 && ar[i]%2!=0){
			noJugadorX[i] -= delta;
			noJugador[i] = noJugadorIzquierda;
		}
		if(obs){
			ar[i] += 1;
			if(noJugadorX[i] > posOriginal[i]){noJugadorX[i] -= 2;}
			else{noJugadorX[i] += 2;}
		}
	}

	private void detectaOro() {
		for (int j = 0; j < oro.length; j++) {
			for (int i = 0; i < oro[j].length; i++) {
				if (oro[j][i] && (jugadorY >= 480 - (32 * (15 - i)) && jugadorY <= 480 - (32 * (14 - i)) && (jugadorX >= 32 * j && jugadorX <= 32 * (j + 1)))) {
					capaOro.getCell(j, i).setTile(null);
					oro[j][i] = false;
					musica2.play();
				}
				if(!oro[10][1] && !oro[24][0] && !oro[21][13] && !oro[21][0] && !oro[16][2] && !oro[5][13]){
					musica2.play();
					new Timer().schedule(new TimerTask() {
						@Override
						public void run() {
							System.exit(0);
						}
					}, 1500);
				}
			}
		}
	}
}
